/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sepia;
import java.io.File;
import java.io.IOException;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

/**
 *
 * @author Martin
 */
public class sepia {

        /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    
    BufferedImage img = null;
    File f = null;
    //leyendo la imagen para el formato rgb
    try{
     f = new File("1bmp.bmp");//f = new File("prueba1.bmp");
      img = ImageIO.read(f);
    }catch(IOException e){
      System.out.println(e);
    }

    //obteniendo el ancho y largo de la imagen
    int width = img.getWidth();
    int height = img.getHeight();

    //Recorremos en la imagen por cada pixel y asignamos el nuevo filtro color verde
    for(int y = 0; y < height; y++){
      for(int x = 0; x < width; x++){
        int p = img.getRGB(x,y);
        int a = (p>>24)&0xff;
        int r = (p>>16)&0xff;
        int g = (p>>8)&0xff;
        int b = p&0xff;
        p = (a<<24) | (r<<16) | (g<<10) | (b<<7)| 0x00;
        img.setRGB(x, y, p);
      }
    }
    
    //Salvamos la imagen con el filtro verde
    try{
      f = new File("1_sepia.bmp");
      ImageIO.write(img, "bmp", f);
      }catch(IOException e){
      System.out.println(e);
    } 
        
        
    }
    
}

