
package filtros;
import java.awt.Image; //representa las imagenes en arreglos de de pixeles
import java.awt.image.BufferedImage; // permite trabajar con los datos de imformacion que hay en los pixeles
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;


public class Filtros {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        BufferedImage img=null;
        try
        {
           img=ImageIO.read(new File("1bmp.bmp"));// img=ImageIO.read(new File("C:\\Users\\MSII\\Desktop\\1bmp.bmp"));
            if(img==null)
            {
                System.out.print("No se leyo la imagen");
            }
            else
            {
                System.out.print(" se leyo la imagen");
            }
        }//try
        catch(IOException e)
        {}//catch
        int anc=img.getWidth();
        int alt=img.getHeight();
        
        for(int x=0; x<alt; x++)
        {
            for(int y=0; y<anc; y++)
            {
                int opc=img.getRGB(x, y);
                int a=(opc>>24)&0xff;
                int r=(opc>>16)&0xff;
                int g=(opc>>8)&0xff;
                int b=0xff;
                int m=(r+g+b)/3;
                opc=(a<<24)|(m<<16)|(m<<8)|m;
                img.setRGB(x,y,opc);
            }//for2
        }//for1
        try
        {
            File k=null;
            k=new File("1bmp_byn.bmp");// k=new File("C:\\Users\\MSII\\Desktop\\1bmp_byn.bmp");
            ImageIO.write(img,"bmp",k);
        }//try
        catch(IOException e)
        {
            System.out.print(e);
        }//catch
        
    }//main
    
}
